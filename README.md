# Doorbell Remote

A simple home automation controller built for a Ritto 17230/.0 TwinBus Freisprechstelle.

![Photo of the first breadboard build](breadboard-photo.jpg)

[See the Hackaday.io project page](https://hackaday.io/project/185420-smart-doorbell-integration-for-ritto-twinbus)


## Introduction

This was designed to be fairly simple, and to use a separate power supply in order to avoid interfering with the
TwinBus system. (apparently TwinBus is rather sensitive to additional drain, so running a relay off the TwinBus power
would probably cause issues with the system)


## Hardware

There are two Fritzing files included:
- [doorbell-remote.fzz]() - An approximation of the initial design I built on a breadboard and deployed
- [doorbell-remote-final.fzz]() - A cleaned up version meant for manufacturing PCBs


### Bill of Materials

- Breadboard, perfboard, or custom PCB
- 12V DC power supply
- `U1`: ESP-01 - Wi-Fi-capable microcontroller (should be seated in a 2x4 female pin header block)
- `U2`: PC817 - Optocoupler
- `M1`: DC-DC buck converter (step-down) - 12V -> 3.3V
- `K1`: 5V SPDT relay
- `J1`: 3-pin header, male
- `Q1`: NPN transistor (TO92, EBC)
- `Power`: DC barrel jack or similar 12V power connector
- `R1`: 1KΩ resistor
- `R2`: 10KΩ resistor
- `R3`: 220Ω resistor


## Software

I used [ESPHome](https://esphome.io/) to program the ESP-01. The configuration file can be found at [doorbell.yaml]().


## License

This hardware design is licensed under the terms of the [Solderpad Hardware License v2.1](https://solderpad.org/licenses/SHL-2.1/).
